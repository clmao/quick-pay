define(function (require, exports, module) {
	var VueCountdown = require('./vue-countdown.min.js');
	var Vue = require('./vue.js');
	var common = require('./common.js');
	Vue.component('comp-count-down', {
		template:
			"			<countdown :time='totleTime'" +
			"		               :auto-start='true'" +
			"		               @countdownstart='countdownStarted'" +
			"		               @countdownprogress='countdownProgressing'" +
			"		               @countdownend='countdownended'>" +
			"		    </countdown> ",
		data: function () {
			return {
				totleTime: 0
			};
		},
		components: {
			'countdown': VueCountdown
		},
		props: {
			inittime: {
				type: Number,
				default: 0
			},
		},
		mounted: function () {
			this.getInitData();
		},
		methods: {
			getInitData: function () {
				var params = {
					orderid: common.getUrlParams().orderid
				};
				common.req('getInitData', params, function (res) {
					if (res.code == 0) {
						var times = (res.data.use_time * 1e3 + (300 * 1e3)) - res.time * 1e3;
						if (times <= 0) {
							alert('二维码已失效，请刷新后重试！');
							return this.totleTime = 0;
						}
						this.totleTime = times;
						this.$emit('initdata', res.data);
					}
				}.bind(this));
			},
			countdownStarted: function () {
				this.$emit('countdownstart');
			},
			countdownProgressing: function (days) {
				if (days.hours < 0) return false;
				this.$emit('countdowndata', {
					hours: days.hours < 10 ? '0' + days.hours : days.hours.toString(),
					minutes: days.minutes < 10 ? '0' + days.minutes : days.minutes.toString(),
					seconds: days.seconds < 10 ? '0' + days.seconds : days.seconds.toString()
				});
			},
			countdownended: function () {
				//the end 
				this.$emit('countdownend');
			}
		}
	});
});
